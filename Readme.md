＜AIITシニアスタートアップ　DIY-TEAM＞

　”ふうまくん”Fusion360 3DモデリングおよびCAMデータ

<..> Modeling Data
 * fuma_assy1v1.png　ふうまくんイメージ
 * fuma_assy1v2.png　ふうまくんイメージ
 * fuma_assy3_drawingv1.pdf　ふうまくん外形図
 * fuma_cam3v3.f3z　ふうまくんFusion360 加工用CAMモデリング
 * body_cam3v9.f3d　ふうまくんボディFusion360 加工用CAMモデリング

<fuma?cam3> G-code
 * 1000_6-ball_backfirst.nc	素材（ストック）裏面荒加工
 * 1001_2dplain.nc		素材表面の2D面出し
 * 1002_round1.nc		ふうまくん裏面等高線加工
 * 1003_ruond2.nc		ふうまくん裏面走査線加工
 * 1004_stockpin.nc		素材裏面位置決めピン穴加工
 * 2001_tablepin.nc		捨て板位置決めピン穴加工
 * 3001_frontmill.nc		素材表面荒加工
 * 3002_round1.nc		ふうまくん表面等高線加工
 * 3003_round2.nc		ふうまくん表面走査線加工
 * 4001_6-ball_badytop.nc	ふうまくん胴体上面加工
 * 4002_6-flatmill1.nc		胴体マグネット穴加工
 * 4003_2-ballmill2.nc		胴体ねじ穴加工
